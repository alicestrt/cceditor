
function trim_parentheses (m) { return m.replace(/^\((.+)\)$/, "$1") }

// nb: trims []:? ... ie optional trailing colon too!
function trim_brackets (m) { return m.replace(/^\[(.+)\]:?$/, "$1") }

function trim_quotes (m) {
    m = m || "";
    return m.replace(/^["(']/, "").replace(/["(']$/, "");
}

function extract_reflink_definitions (text) {
    var ret = {},
        pat = /^\[([^\]]+)\]:\s+([^ \n]+)(?: +("[^"]*"|\'[^"]*\'|\([^\)]*\)|.*))?$/gm;
    text.replace(pat, function (m, label, url, title) {
        var mobj = {label: label, url: url, title: trim_quotes(title)};
        // console.log("extract reflink", mobj);
        ret[label.toLowerCase()] = mobj; 
    })
    return ret;
}

function xpandRefLink (href, reflinks) {
    if (href.indexOf("#") >= 0) {
        var hrefp = href.split("#", 2);
        var base = hrefp[0];
        var fragment = hrefp[1];
        var reflink = reflinks[base.toLowerCase()];
        if (reflink !== undefined) {
            // reflink DEFINED!!!!
            // console.log("REFLINK", href, reflink.url)
            return {href: reflink.url+"#"+fragment, title: reflink.title};
        }
    } else {
        // expand non hashes too ?!
        var reflink = reflinks[href.toLowerCase()];
        if (reflink !== undefined) {
            return {href: reflink.url, title: reflink.title};
        }        
    }
    // return {href: href};
}

function compactRefLink (href, reflinks) {
    // console.log("compactRefLink", href);
    var hrefb, hsh = '';
    if (href.indexOf("#") >= 0) {
        var hrefp = href.split("#", 2);
        hrefb = hrefp[0];
        hsh = hrefp[1];
    } else {
        hrefb = href;
    }
    for (var label in reflinks) {
        var v = reflinks[label];
        // console.log("checking", v.url, hrefb);
        if (v.url == hrefb) {
            // console.log("MATCH", v.label);
            return v.label + (hsh ? "#"+hsh : "");
        }
    }
    return href;
}


module.exports = {
    trim_quotes: trim_quotes,
    trim_brackets: trim_brackets,
    trim_parentheses: trim_parentheses,
    extract_reflink_definitions: extract_reflink_definitions,
    xpandRefLink: xpandRefLink,
    compactRefLink: compactRefLink
}
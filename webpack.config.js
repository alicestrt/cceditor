const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  entry: './src/cceditorapp.js',
  mode: 'production', // 'development', production
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'cceditorapp.js',
    library: 'cceditorapp'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" }
        ]
      }
    ]
  }
  // plugins: [
  //     new UglifyJSPlugin()
  // ]
};
